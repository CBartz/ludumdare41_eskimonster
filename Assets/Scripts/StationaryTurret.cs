﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationaryTurret : MonoBehaviour {

	public GameObject projectilePrefab;

	public int ready = 0;
    public int fireInterval = 180;

	Transform tf;
    SpriteRenderer sr;

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform> ();
        sr = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (ready == fireInterval) {
			ready = 0;

			GetComponent<SoundControler>().EnemyShootSound();

			GameObject projectile = Instantiate (projectilePrefab, transform);
			Rigidbody2D projectileRb = projectile.GetComponent<Rigidbody2D>();
			if (sr.flipX) {
				projectileRb.velocity = new Vector2(-5.0f, 0.0f);
			} else {
				projectileRb.velocity = new Vector2(5.0f, 0.0f);
			}
		} else {
			ready++;
		}
	}

}
