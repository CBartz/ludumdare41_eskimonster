﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    List<Transform> LevelList;

    int currentLevel, lastLevel;

	// Use this for initialization
	void Start () {
        //Catalog all levels
        LevelList = new List<Transform>();

        //Find all immediate children. These SHOULD be Level Definitions.
        foreach (Transform child in transform)
        {
            LevelList.Add(child);
        }
        Debug.Log("LevelManager: Found " + LevelList.Count + " levels.");

        currentLevel = 0;
		lastLevel = LevelList.Count;
	}

    //Advance to next level
    public void NextLevel() {
        LevelList[currentLevel].gameObject.SetActive(false); //Unload current level
        currentLevel++;
        if(currentLevel < lastLevel)
        {
            Debug.Log("Advancing to next Level");
			LevelList[currentLevel].gameObject.SetActive(true); //Load next level if there is one
        }
        else
        {
            Debug.Log("No next level, triggering win condition.");
            //trigger YOU WIN condition
		}
		GameObject.Find ("TerminalUserInputLine").GetComponent<TerminalUserInput> ().ReconnectPlayer ();
    }

	public void ResetLevel() {
		GameObject[] projectiles = GameObject.FindGameObjectsWithTag ("Projectile");
		for (int i = 0; i < projectiles.Length; i++) {
			Destroy (projectiles [i]);
		}

		PlayerController player = GameObject.Find ("Player").GetComponent<PlayerController>();
		Rigidbody2D rb = GameObject.Find ("Player").GetComponent<Rigidbody2D> ();
		player.transform.position = player.startPosition;
		player.Stop ();
		rb.velocity = new Vector3 (0f, 0f, 0f);

		Debug.Log (rb.velocity);

		GameObject[] keycards = GameObject.FindGameObjectsWithTag ("KeyCard");
		GameObject.Find ("Level_Exit").GetComponent<ExitDoor> ().resetDoor ();
		for (int i = 0; i < keycards.Length; i++) {
			Keycard kc = keycards [i].GetComponent<Keycard> ();
			keycards [i].SetActive (true);
			kc.transform.position = kc.startPosition;
		}
	}

	
}
