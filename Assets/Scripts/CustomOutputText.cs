﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomOutputText : MonoBehaviour {
    [TextArea]
    public string CustomText = "Custom text goes here.";

    public string getText()
    {
        return CustomText;
    }
}
