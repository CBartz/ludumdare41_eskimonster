﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public enum MoveState
    {
        Stop, Left, Right
    };

	MoveState moveState = MoveState.Stop;

    bool inAir = false;

    public float moveForce = 1.0f;
    public float jumpMagnitude = 5.0f;
    Animator anim;

    Transform tf;
    Rigidbody2D rb;
    SoundControler sc;

	public Vector3 startPosition;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        tf = GetComponent<Transform>();

        rb = GetComponent<Rigidbody2D>();

        sc = GetComponent<SoundControler>();

		startPosition = this.transform.position;
    }

	void Update() {
		switch (moveState) {
		case MoveState.Stop:
			rb.velocity = new Vector2(0.0f, rb.velocity.y);
			break;
		case MoveState.Left:
			rb.velocity = new Vector2(-moveForce, rb.velocity.y);
			break;
		case MoveState.Right:
			rb.velocity = new Vector2(moveForce, rb.velocity.y);
			break;
		}
	}

    public void Jump()
    {
       
        if (!inAir)
        {
            inAir = true;

			rb.velocity = new Vector2(rb.velocity.x, jumpMagnitude);
            sc.PlayerJumpSound();
            //rb.AddForce(jumpMagnitude, ForceMode2D.Impulse);
        }

    }

    public void MoveLeft()
    {
        anim.SetBool("Moving", true);
        // look left
        if(transform.localScale.x > 0)
        {
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y);
        }
        

        if (!inAir)
		{
			moveState = MoveState.Left;
			rb.velocity = new Vector2(-moveForce, rb.velocity.y);
		}
    }

    public void MoveRight()
    {
        anim.SetBool("Moving", true);
        // look right

        if (transform.localScale.x < 0)
        {
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y);
        }
       


        if (!inAir)
		{
			moveState = MoveState.Right;
			rb.velocity = new Vector2(moveForce, rb.velocity.y);
		}
    }

    public void Stop()
    {
        anim.SetBool("Moving", false);
		moveState = MoveState.Stop;
        rb.velocity = new Vector2(0f, rb.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            inAir = false;
            sc.PlayerLandSound();
        }
        if(collision.gameObject.tag == "Projectile")
        {
			GameObject.Find ("Levels").GetComponent<LevelManager> ().ResetLevel ();
			sc.PlayerHurtSound ();
			TerminalOutput.PrintLine ("You Died!");

        }
    }
}
