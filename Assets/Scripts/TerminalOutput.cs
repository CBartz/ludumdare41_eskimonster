﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TerminalOutput : MonoBehaviour {

	static Text output;

	// Use this for initialization
	void Start () {
		output = gameObject.GetComponent<Text>();

		output.text = "Eskimonster Presents:\nA TEXT BASED PLATFORMER by ARCH, KEVIN-P, MIKUNE, and ORION-SUNI-SAHARA\nColossal Cat Adventure: Maze of the Elder Gods: Lazer Turret's Revenge Part 2: The Awakening! \n\nType “help” for full list of commands.";
	}

	public static void Help() {
		PrintLine ("help - display this info  |  left - move left\nright - move right        |  stop - stop moving\njump - jump...            |  grab - pick up keycards\nuse - use terminals       |  enter - enter doors\npause/unpause - pause/unpause the game\nrestart and quit");
	}

	public static void PrintLine(string line) {
		output.text = output.text + "\n" + line;
	}
	// Update is called once per frame
	void Update () {
		
	}
}
