﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableTerminal : MonoBehaviour {

    CustomOutputText outputText;
	public bool usable;
	// Use this for initialization
	void Start () {
        outputText = GetComponent<CustomOutputText>();
	}

	//Maybe convert this to onTriggerEnter2D and onTriggerLeave2D for more state-based approach?
	void OnTriggerEnter2D(Collider2D other)
	{

        if (other.tag == "Player")
        {
            //Somehow tie to terminal command for "USE"
            usable = true;
            TerminalOutput.PrintLine("Type Use to read terminal information."); //returns custom text to be put in the terminal.
        }
    }

	public bool TouchingPlayer(){
		Collider2D playerHitBox = GameObject.Find ("Player").GetComponent<Collider2D> ();
		if (this.GetComponent<Collider2D> ().IsTouching (playerHitBox)) {
			return true;
		}
		return false;
	}

	public string getText() {
		return outputText.getText ();
	}
}
