﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControler : MonoBehaviour {

    AudioSource player;

    public  AudioClip BadComm, DoorOpen, EnemyShoot, Land, Pickup, PlayerHurt, PlayerJump;

    void Start()
    {
        player = GetComponent<AudioSource>();
    }

    public void PlayerJumpSound()
    {
        player.PlayOneShot(PlayerJump);
    }

    public void PlayerLandSound()
    {
        player.PlayOneShot(Land);
    }

    public void PlayerHurtSound()
    {
        player.PlayOneShot(PlayerHurt);
    }

    public void BadCommandSound()
    {
        player.PlayOneShot(BadComm);
    }

    public void EnemyShootSound()
    {
        player.PlayOneShot(EnemyShoot);
    }

    public void PickupKeySound()
    {
        player.PlayOneShot(Pickup);
    }

    public void DoorOpenSound()
    {
        player.PlayOneShot(DoorOpen);
    }
}
