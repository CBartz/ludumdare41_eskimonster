﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keycard : MonoBehaviour {
	public Vector3 startPosition;	
	void Start()
	{
		startPosition = this.transform.position;
	}
	public bool TouchingPlayer(){
		Collider2D playerHitBox = GameObject.Find ("Player").GetComponent<Collider2D> ();
		if (this.GetComponent<Collider2D> ().IsTouching (playerHitBox)) {
			return true;
		}
		return false;
	}

	public void GrabKey() {
		GetComponentInParent<ExitDoor>().pickupKeycard();
		gameObject.SetActive(false);
	}
}
