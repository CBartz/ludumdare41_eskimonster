﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TerminalUserInput : MonoBehaviour {

    SoundControler sc;

	TerminalOutput output;
	InputField inputText;
	PlayerController controlObject;
	InteractableTerminal it;
	ExitDoor exitDoor;

	// Use this for initialization
	void Start () {
        sc = GetComponent<SoundControler>();

		exitDoor = GameObject.Find ("Level_Exit").GetComponent<ExitDoor> ();
		controlObject = GameObject.Find ("Player").GetComponent<PlayerController> ();
		it = GameObject.Find ("Terminal_Interactable").GetComponent<InteractableTerminal> ();
		inputText = gameObject.GetComponent<InputField>();
		inputText.onEndEdit.AddListener(SubmitCommand);
		inputText.ActivateInputField ();
	}

	private void SubmitCommand(string arg0)
	{
        arg0 = arg0.ToLower ();
		switch (arg0) {
		case "left":
			TerminalOutput.PrintLine ("Moving Left");
			controlObject.MoveLeft ();
			break;
		case "right":
			TerminalOutput.PrintLine("Moving Right");
			controlObject.MoveRight ();
			break;
		case "jump":
			TerminalOutput.PrintLine ("Jumping");
			controlObject.Jump ();
			break;
		case "stop":
			TerminalOutput.PrintLine ("Stopping");
			controlObject.Stop ();
			break;
		case "grab":
			bool foundCard = false;
			GameObject[] keycards = GameObject.FindGameObjectsWithTag ("KeyCard");
			for (int i = 0; i < keycards.Length; i++) {
				Keycard currentCard = keycards [i].GetComponent<Keycard> ();
				if (currentCard.TouchingPlayer ()) {
					TerminalOutput.PrintLine ("Grabbed Keycard");
					sc.PickupKeySound ();
					currentCard.GrabKey ();
					foundCard = true;
				}
			}
			if (!foundCard) {
				TerminalOutput.PrintLine ("No keycard to grab here");
			}
			break;
		case "use":
			if (it.TouchingPlayer ()) {
				TerminalOutput.PrintLine(it.getText());
			} else {
				TerminalOutput.PrintLine ("Nothing to use here.");
			}
			break;
		case "enter":
			if (exitDoor.TouchingPlayer ()) {
                sc.PickupKeySound();
                exitDoor.enterDoor ();
			}else {
                    TerminalOutput.PrintLine("No door here.");
                }
                break;
		case "pause":
			TerminalOutput.PrintLine ("Pausing");
			Time.timeScale = 0;
			break;
		case "unpause":
			TerminalOutput.PrintLine ("Unpausing");
			Time.timeScale = 1;
			break;
		case "quit":
			TerminalOutput.PrintLine ("Quitting");
			Application.Quit ();
			break;
		case "help":
			TerminalOutput.Help();
			break;
		case "restart":
			GameObject.Find ("Levels").GetComponent<LevelManager> ().ResetLevel ();
			break;
		default:
			TerminalOutput.PrintLine ("Unknown command " + arg0);
                sc.BadCommandSound();
			break;
		};
		inputText.text = "";
		inputText.ActivateInputField ();
		//GUI.FocusControl("TerminalUserInputLine");
	}

	public void ReconnectPlayer() {
		exitDoor = GameObject.Find ("Level_Exit").GetComponent<ExitDoor> ();
		controlObject = GameObject.Find ("Player").GetComponent<PlayerController> ();
		it = GameObject.Find ("Terminal_Interactable").GetComponent<InteractableTerminal> ();
	}

	// Update is called once per frame
	void Update () {
		
	}
}
