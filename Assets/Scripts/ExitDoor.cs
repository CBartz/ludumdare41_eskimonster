﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitDoor : MonoBehaviour {

    public int keycardsRemaining = 0;
    SpriteRenderer sr;
    public Sprite unlockedDoor, lockedDoor;
	Keycard[] keycards;
    //Find all keycards in level
    void Start()
    {
        keycards = GetComponentsInChildren<Keycard>();
        keycardsRemaining = keycards.Length;
        Debug.Log(keycardsRemaining + " keycards found in level.");

        //If there are no keycards, swap to unlocked door.
        sr = GetComponent<SpriteRenderer>();
        if(keycardsRemaining == 0)
        {
            sr.sprite = unlockedDoor;
        }
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {
		if (other.tag == "Player") {
			TerminalOutput.PrintLine ("If the door is unlocked type enter to go to the next level!");
		}
    }

    public void pickupKeycard()
    {
        keycardsRemaining--;
		unlockDoor ();
    }

	public void enterDoor() {
		if (sr.sprite == unlockedDoor) {
			TerminalOutput.PrintLine ("Going to next level");
			GameObject.Find ("Levels").GetComponent<LevelManager> ().NextLevel ();
		} else {
			TerminalOutput.PrintLine ("You need to unlock the door first!");
		}
	}

	public void unlockDoor()
	{
		if (keycardsRemaining == 0) {
			TerminalOutput.PrintLine ("Unlocked Door");
			sr.sprite = unlockedDoor;
		} else {

			TerminalOutput.PrintLine ("Keycards remaining: " + keycardsRemaining);
		}
	}

	public bool TouchingPlayer(){
		Collider2D playerHitBox = GameObject.Find ("Player").GetComponent<Collider2D> ();
		if (this.GetComponent<Collider2D> ().IsTouching (playerHitBox)) {
			return true;
		}
		return false;
	}

	public void resetDoor(){
		Debug.Log (keycards.Length);
		keycardsRemaining = keycards.Length;
		for (int i = 0; i < keycards.Length; i++) {
			keycards [i].gameObject.SetActive (true);
		}
		if (keycardsRemaining == 0) {
			sr.sprite = unlockedDoor;
		} else {
			sr.sprite = lockedDoor;
		}
	}
}
